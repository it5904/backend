import CMSModel from '@entities/mongo-entities/CMS/cms.entity';

export class CMSRepository {
  async getList(): Promise<any> {
    const data = await CMSModel.find();
    console.log(33333, data);
    
    return data;
  }

  async getDetail(code: string): Promise<any> {
    const data = await CMSModel.findOne({ code: code });

    return [{ attributes: data }];
  }

  async getDetailMongo(addressSC: string): Promise<any> {
    const data = await CMSModel.findOne({ SC_collection: addressSC });

    return data;
  }

  async addCMS(data: any): Promise<any> {
    const res = await CMSModel.findOneAndUpdate(
      {
        SC_collection: data.SC_collection,
      },
      {
        $set: { ...data },
      },
      { upsert: true, new: true },
    );

    return res;
  }
}
