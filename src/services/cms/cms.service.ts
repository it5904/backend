/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import 'reflect-metadata';
import { ApiError } from '@models/api-error';
import { Service } from 'typedi';
import { ResponseCodeEnum } from '@models/enums/response-code.enum';
import { StatusCodes } from 'http-status-codes';
import { CMSRepository } from '@repositories/cms/cms.repository';

@Service()
export class CMSService {
  private readonly cmsRepository: CMSRepository;

  constructor() {
    this.cmsRepository = new CMSRepository();
  }

  async addCMS(data: any): Promise<any> {
    const res = await this.cmsRepository.addCMS(data);

    if (!res)
      throw new ApiError(StatusCodes.BAD_REQUEST, ResponseCodeEnum.CM0018);
    return res;
  }

  async getList(): Promise<any> {
    console.log(22222);
    const res = await this.cmsRepository.getList();
    
    if (!res)
      throw new ApiError(StatusCodes.BAD_REQUEST, ResponseCodeEnum.CM0001);
    return res;
  }

  async getDetail(code: string): Promise<any> {
    const res = await this.cmsRepository.getDetail(code);

    if (!res)
      throw new ApiError(StatusCodes.BAD_REQUEST, ResponseCodeEnum.CM0001);
    return res;
  }

  async getDetailMongo(addressContract: string): Promise<any> {
    const res = await this.cmsRepository.getDetailMongo(addressContract);

    if (!res)
      throw new ApiError(StatusCodes.BAD_REQUEST, ResponseCodeEnum.CM0001);
    return res;
  }
}
