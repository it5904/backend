import {MigrationInterface, QueryRunner} from "typeorm";

export class updateTableCollectionFloorPrice1687397954151 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "collections" 
            ALTER COLUMN floor_price type BIGINT
            USING floor_price::bigint;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TABLE "collections" 
        ALTER COLUMN floor_price type text;
        `);
    }
}
