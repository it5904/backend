import { validateMiddleware } from '@core/middleware/validate.middleware';
import { ResponseBuilder } from '@core/utils/response-builder';
import Router from '@koa/router';
import { AppState } from '@models/app.state';
import { CMSRequest } from '@models/cms/cms.request';
import { APIEnum } from '@models/enums/api.category.enum';
import { CMSService } from '@services/cms/cms.service';
import { ParameterizedContext } from 'koa';
import Container from 'typedi';
import { Context } from 'vm';

const CMSRoute = new Router({
  prefix: '/cms',
});

// Add CMS
CMSRoute.post(
  APIEnum.ADD_CMS,
  validateMiddleware({
    body: CMSRequest,
  }),
  async (ctx: ParameterizedContext<AppState, Context>): Promise<void> => {
    const cmsService = Container.get<CMSService>(CMSService);
    const data = await cmsService.addCMS(ctx.request.body);
    ctx.body = new ResponseBuilder(data).build();
  },
);

CMSRoute.get(
  APIEnum.GET_LIST_CMS,
  async (ctx: ParameterizedContext<AppState, Context>): Promise<void> => {
    console.log(11111);
    const cmsService = Container.get<CMSService>(CMSService);
    
    const data = await cmsService.getList();
    ctx.body = new ResponseBuilder(data).build();
  },
);

CMSRoute.post(
  APIEnum.GET_DETAIL_CMS,
  async (ctx: ParameterizedContext<AppState, Context>): Promise<void> => {
    const cmsService = Container.get<CMSService>(CMSService);
    const data = await cmsService.getDetail(ctx.request.body.code);
    ctx.body = new ResponseBuilder(data).build();
  },
);

CMSRoute.post(
  APIEnum.GET_DETAIL_CMS_MONGO,
  async (ctx: ParameterizedContext<AppState, Context>): Promise<void> => {
    const cmsService = Container.get<CMSService>(CMSService);
    const data = await cmsService.getDetailMongo(ctx.request.body.SC_collection);
    ctx.body = new ResponseBuilder(data).build();
  },
);

export { CMSRoute };
