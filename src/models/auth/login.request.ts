import { IsNotEmpty, IsOptional } from 'class-validator';
import { NetworkTypeEnum } from '../enums/netwok.enum';

export class LoginRequest {
  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  signature: string;

  @IsNotEmpty()
  network: NetworkTypeEnum;

  @IsOptional()
  messageBytes: string;
}
