import { IsNotEmpty } from 'class-validator';

export class CMSRequest {
  @IsNotEmpty()
  code: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  banner: string;

  @IsNotEmpty()
  logo: string;

  @IsNotEmpty()
  whitelistStartTime: number;

  @IsNotEmpty()
  whitelistEndTime: number;

  @IsNotEmpty()
  publicStartTime: number;

  @IsNotEmpty()
  publicEndTime: number;

  @IsNotEmpty()
  collectionStatus: string;

  @IsNotEmpty()
  collectionCategory: string;

  @IsNotEmpty()
  SC_collection: string;

  @IsNotEmpty()
  SO_collection: string;

  @IsNotEmpty()
  itemCount: string;

  @IsNotEmpty()
  maxWhitelistMint: string;

  @IsNotEmpty()
  maxPublicMint: string;

  @IsNotEmpty()
  priceWhitelist: string;

  @IsNotEmpty()
  pricePublic: string;

  @IsNotEmpty()
  publicAccountLimit: string;

  @IsNotEmpty()
  whitelistAccountLimit: string;
}
