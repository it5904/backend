import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const CMSSchema = new Schema(
  {
    name: {
      type: String,
    },
    function: {
      type: String,
    },
    banner: {
      type: String,
    },
    logo: {
      type: String,
    },
    code: {
      type: String,
    },
    whitelistStartTime: {
      type: Number,
    },
    whitelistEndTime: {
      type: Number,
    },
    publicStartTime: {
      type: Number,
    },
    publicEndTime: {
      type: Number,
    },
    collectionStatus: {
      type: String,
    },
    collectionCategory: {
      type: String,
    },
    SC_collection: {
      type: String,
    },
    SO_collection: {
      type: String,
    },
    itemCount: {
      type: String,
    },
    maxWhitelistMint: {
      type: String,
    },
    maxPublicMint: {
      type: String,
    },
    priceWhitelist: {
      type: String,
    },
    pricePublic: {
      type: String,
    },
    publicAccountLimit: {
      type: String,
    },
    whitelistAccountLimit: {
      type: String,
    },
    typeTicket: {
      type: String,
    },
  },
  { collection: 'CMS' },
);
const CMSModel = mongoose.model<mongoose.Document>('CMS', CMSSchema);
export { CMSModel, CMSSchema };
export default CMSModel;
